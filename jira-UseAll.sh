#!/bin/bash
##############Requirements###############
#jq
#Curl
#Dependent issueLink
#
#Create issueLink with :
#POST http://localhost:8080/rest/api/latest/issueLinkType
#{
#    "name": "Dependent",
#    "inward": "Depends on",
#    "outward": "is depended on by"
#}
#
################Functions###############
#Yes no question
function yesNoQ() {
		if [ "$1" = "y" ]
		then
			echo "true";
		elif [ "$1" = "yes" ]
		then
			echo "true";
		else
			echo "false";
		fi
}

#Gets the newly created key of certain POST functions
function getKey(){
		taskKey=$(jq -r '.key' <<< "$response");

        #Check for error creating main task
        if [[ $taskKey == null ]];
        then
                echo "Error creating task...";
                echo "Post: $postBody";
                echo "Response: $response";
                echo "Exiting...";
                exit 1;
        fi
}

#Creates the epic
function createEpic(){

        echo "Creating epic..."
        postBody=$(cat json/createEpic);
        postBody="${postBody/KEYVAL/$keyVal}";
        postBody="${postBody//NAMEVAL/$username}";

        response=$(curl -u "$adminUser:$adminPass" -d "$postBody" -H "$contentType"  "${baseURI}/issue");
}


#Creates a new issue
function createNewIssue(){
        echo "Creating issues from $1...";
        postBody=$(cat "$1");
        postBody="${postBody//PKEYVAL/$2}";
        postBody="${postBody//KEYVAL/$keyVal}";
        postBody="${postBody//NAMEVAL/$username}";

        tempURI="${baseURI}/issue";
        if [[ $2 != "" ]];
        then
                tempURI="${baseURI}/issue/bulk";
        fi

        response=$(curl -u "$adminUser:$adminPass" -d "$postBody" -H "$contentType"  "${tempURI}");
        echo "Done with $1 file...";
}

#Links Story issues to Epic as a dependant
function linkIssues(){
		echo "Creating link to Epic..."
        postBody=$(cat json/other/issueLink);
        postBody="${postBody/CHILD/$taskKey}";
        postBody="${postBody/PARENT/$epicKey}";

        response=$(curl -u "$adminUser:$adminPass" -d "$postBody" -H "$contentType"  "${baseURI}/issueLink");
        echo "Link created...";
}

###########TestData#####################
keyVal="<Project Keyval>";

username="<Asignee username>"
adminUser="<Admin username>";
adminPass="<Admin password (I recommend using a token)>";
base="http://localhost:8080";
###############Ask for data##############
echo "Input the username for the tasks:";
read username;

echo "Input a key value for the project:";
read keyVal;

echo "Input Admin username:"
read adminUser;

echo "Iput Admin password OR token:"
read adminPass;

echo "Input base host: (ex http://localhost:8080)"
read base

#confirm the data
echo "Username: $username";
echo "Project Key: $keyVal";
echo "Host: $base";

#Check to continue
echo "Is this Correct?(y/n)";
read ans;
good=$(yesNoQ "$ans");

############Global vars##################
baseURI="${base}/rest/api/latest";
loginURI="${base}/rest/auth/1/session"
contentType="Content-Type: application/json";
###################Data is good#######################
if [[ $good == true ]];
then

		#Creates the bootcamp epic
		createEpic;
		#returns the key of the epic
		getKey;
		epicKey="$taskKey";
	
		#Loop through all files in json directory
		for i in ./json/*;
		do
			#Remove directory names
	        file=${i:7};
	
			#Skip if its the Sub sub-directory
	        if [[ $file != "sub" ]] && [[ $file != "other" ]] && [[ $file != "createEpic" ]]
	        then

				#Create specified storye
	           	createNewIssue "json/$file";
	
				#Get the key it created
				getKey;
		
				#Link issue to Epic
				linkIssues;
		
				#check if a subFile exists
	            subFile="json/sub/${file}Sub";
	        	if [[ -f "$subFile" ]];
		        then
					#Create specified sub-tasks
					createNewIssue "$subFile" "$taskKey";
		     	fi
	      	fi
		done
fi
