# On Boarding Script
## Summary
This script was created to help with new on-boarding trainee's at Mountain State Software Solutions. The
purpose was to make a simple but easily populated checklist for the new employees to review and mark tasks as done as they go.
This also doubles as a small introduction into how JIRA issues work

## Requirements
#### Curl: https://curl.haxx.se/download.html
#### JQ : https://stedolan.github.io/jq/download/
#### 'Dependent' Issue Link: The link between Epics and Stories are created with an Issue Link called dependent.
* If you would like to use a different link change it in the /json/other/issueLink file
* Or you can create the dependent link with:  
`
POST /rest/api/latest/issueLinkType
{
	"name": "Dependent",
	"inward": "Depends on",
	"outward": "is depended on by"
}
`
## General Flow

1. Get input data
	* Asignee User
	* Project key value
	* Posters username
	* Posters password/token
	* Base Host
2. Confirm Data
3. Create Epic
	* Read epic from json/createEpic
4. Store epic key
5. Loop through other json files
	* Create Story
	* Link story to Epic
	* Creat sub-tasks with Sub file

## Issues
I have noticed is there is an object in the JSON for creating Epics called "customField_#####" (where # is a number). 
On my local JIRA this number was 10003, on my Cloud version this number is 10011. 
I'm not sure what determines the number or what the given value affects (I have it set as Name and I don't see Name anywhere). 
The only way I know how to figure out the number is by running the post command, it will return with customfield_##### cannot be empty.
## UseOrdered
### Summary
The UseOrdered version was created specifically for MS3's boot camp, it uses the json files included in a specific order so 
that the stories and sub tasks are all in the correct order.
## UseAll
### Summary
The UseAll version was created to be a bit more abstract, this version uses createEpic file to create the epic then parses 
all files under /json (ignoring the /sub /other and createEpic) to create the Epic's sub stories. Using the story name, it
checks for a sub file with the same name example story:`Story=/json/exStroy` and that stories sub-tasks:`SubTask=/json/sub/exStorySub`
if the sub file exists it uses that file to generate the stories sub tasks.

Example file structure:
```
Root/
|
--jira.sh
--json/
  |
  --createEpic
  --story
  --sub/
    |
	--storySub
```
### Creating JSON file
Since the UseAll section is meant to customize your own Epic, Stories, and Sub-Tasks you will probably want to make your own JSON files for creating these.
This script is meant to only create one Epic so for that I would just recommend editing the /json/createEpic file. The "Summary" key affects the title of the epic
and the "description" key obviously changes the description of the Epic.

For creating a story, you will want to put the file in the json/ folder. The JSON itself should make use of the keyword `KEYVAL` for the project key and the key word
`NAMEVAL` for the assignee's username.

Story example:
```json
{
	"fields": {
		"project": { 
			"key": "KEYVAL"
		},
		"summary": "My Story Title",
		"description": "My description",
		"issuetype": {
			"name": "Story"
		},
                "assignee": {
                        "name": "NAMEVAL"
                }

	}
}
```

Finally for Sub-Tasks, you will want to put the JSON files under the json/sub/ folder. The JSON itself should make use of the keyword `KEYVAL` for the project key, the key word `PKEYVAL` for the parent Story,
and the key word `NAMEVAL` for the assignee's username. You can add any number of "field" objects as you need as long as you use atleast one.
```json
{
	"issueUpdates": [
		{
			"fields":{
				"project":{
					"key": "KEYVAL"
				},
				"parent":{
					"key": "PKEYVAL"
				},
				"summary": "Sub-Task Title",
				"description": "Description",
				"issuetype":{
					"name": "Sub-task"
				},
                "assignee": {
                	"name": "NAMEVAL"
                 } 
		},
		{
			"fields":{
				"project":{
					"key": "KEYVAL"
				},
				"parent":{
					"key": "PKEYVAL"
				},
				"summary": "Sub-Task Title",
				"description": "Description",
				"issuetype":{
					"name": "Sub-task"
				},
                "assignee": {
                	"name": "NAMEVAL"
                 } 
			}
		}
	]
}
```
## Demo
Example usage

Run script: **./jira-<type>.sh**  
Input the username for the tasks: (username of the asignee user)  
**TESTUSER**  
Input a key value for the project: (key value of the project, must be CAPS)  
**TEST**  
Input Admin email: (Username of someone with permission to add tasks))  
**ADMIN@example.com**  
Input Admin password OR token: (that persons password or token, if you have 2 fator authentication Use an API Token)  
**TOKEN**  
Input base host: (ex http://localhost:8080)  
**http://localhost:8080**  
Username: **TESTUSER**  
Project Key: **TEST**  
Host: **http://localhost:8080**  
Is this Correct?(y/n)  (prints out data for confirmation, input 'y' or 'yes' to continue)  

Enjoy your new tasks!